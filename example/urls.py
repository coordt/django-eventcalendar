# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^email-sent/', 'example.app.views.validation_sent'),
    url(r'^login/$', 'example.app.views.social_login'),
    url(r'^logout/$', 'example.app.views.logout'),
    url(r'^done/$', 'example.app.views.done', name='done'),
    url(r'^ajax-auth/(?P<backend>[^/]+)/$', 'example.app.views.ajax_auth', name='ajax-auth'),
    url(r'^email/$', 'example.app.views.require_email', name='require_email'),
    url(r'^$', TemplateView.as_view(template_name="homepage.html"),),
    url(r'^events/', include('eventcalendar.urls')),
    url(r'^search/', include('haystack.urls')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(
            r'^static/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )
