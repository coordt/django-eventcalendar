# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage
import eventcalendar.fields


class Migration(migrations.Migration):

    dependencies = [
        ('eventcalendar', '0004_add_external_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image',
            field=models.FileField(upload_to=b'eventcalendar/events/images', storage=django.core.files.storage.FileSystemStorage(), max_length=255, blank=True, null=True, verbose_name='event image'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='more_info_url',
            field=eventcalendar.fields.OptionalSchemeURLField(null=True, verbose_name='URL for more info', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='tickets_url',
            field=eventcalendar.fields.OptionalSchemeURLField(null=True, verbose_name='URL for tickets', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='venue',
            name='image',
            field=models.FileField(upload_to=b'eventcalendar/venues/images', storage=django.core.files.storage.FileSystemStorage(), max_length=255, blank=True, null=True, verbose_name='image'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='venue',
            name='website',
            field=eventcalendar.fields.OptionalSchemeURLField(max_length=255, null=True, verbose_name='web site', blank=True),
            preserve_default=True,
        ),
    ]
