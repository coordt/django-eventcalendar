from django.apps import AppConfig


class EventCalendarConfig(AppConfig):
    name = 'eventcalendar'
    verbose_name = "event calendar"
